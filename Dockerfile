FROM node:8.12.0-jessie

RUN apt update && apt install python

RUN mkdir coresciences

COPY . ./docs.solinr.net

RUN cd docs.solinr.net && \
      yarn && yarn build

EXPOSE 80/tcp

CMD cd docs.solinr.net && node_modules/.bin/serve -l 80 public/
