---
prev: /
next: /services/
---

# What is this?

This site is where we store everything **Solinr!** We believe that transparency is key, hence why we have everything we do in these docs. You will find our services, products, guides, and even our employee handbook here!

This is a very recent development in our history, hence the absence of a lot of our content, but we're working on it!

For now, why don't you go check out our [services](/services/) section? Don't worry, we'll be sure to update you if anything changes. ;)
