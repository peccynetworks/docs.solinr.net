module.exports = {
    title: 'Solinr Docs',
    description: 'Information, Services, & More',
    base: '/',
    dest: 'public',
    head: [
        ['link', {
            rel: 'icon',
            href: '/logoWhiteOnBlueCircle.png'
        }]
    ],
    serviceWorker: true,
    themeConfig: {
        sidebar: {
          '/services/': [
            ['/services/', 'Services'],
		        'shared-webhosting'
          ]
        },
        sidebarDepth: 4,
        displayAllHeaders: false,
        activeHeaderLinks: false,
        lastUpdated: true,
        nav: [{
            text: 'Services',
            items: [{
                    text: 'Shared Webhosting',
                    link: '/services/shared-webhosting'
                }
            ]
        }],
        serviceWorker: {
            updatePopup: {
                message: 'This page just got updated!',
                buttonText: 'Refresh?'
            }
        },
        repo: 'https://gitlab.com/peccynetworks/docs.solinr.net',
        repoLabel: 'I\'m Open Source!',
        docsDir: 'docs',
        //editLinks: true,
        //editLinkText: 'Help us improve this page!',
        // algolia: {
        //     apiKey: 'ad618428dcffec7d35c9f77b544b1d9a',
        //     indexName: 'uagpmc'
        // }
    },
    markdown: {
        lineNumbers: true
    }
}
