---
prev: /services/
---

# Shared Webhosting

> Who says good web hosting needs to be complicated and expensive? If you’re looking for a simple hosting environment and don’t need all the bells and whistles, then this is the perfect service for you!

## Features

**Affordable**

Utilising a powerful combination of network load balancing and efficient cluster management, we have been able to create the most cost-effective shared hosting network to date, all without sacrificing performance.

**Powerful**

Backed by hyperscalers such as Google, Amazon, and Microsoft, we are able to levy some serious hardware and network bandwidth to ensure all of our services are comparable to other top-tier budget hosting providers.

**Secure**

Most budget hosting providers skimp out on this one, but we here at Solinr have adopted a “security-centric” design philosophy when it comes to our solutions and services. From advanced DDoS protection to bulletproof servers, we never back down from a fight to save a buck.

## Pricing

|                    | Static | Custom | Dynamic |
|-------------------:|:------:|:------:|:-------:|
|      Load-Balanced |    ✓   |    ✓   |    ✓    |
|        FTPS Access |    ✓   |    ✓   |    ✓    |
|    LetsEncrypt SSL |    ✓   |    ✓   |    ✓    |
| Custom PHP Version*|        |    ✓   |    ✓    |
|         Subdomains |        |   10   |    ✓    |
|         SSH Access |        |        |    ✓    |
|    MySQL Instance**|        |        |    ✓    |
| Price _(per month)_|   £1   |  £2.50 |    £5   |

`* Available version of PHP are: 5.3, 5.4, 5.5, 5.6, 7.0, 7.1, 7.2`

`** MySQL instances are limited to 1024MB of storage`

## How does it work?

We incorporate the latest and greatest technology to provide this service:

**Global Load Balancing**

“Load balancing” refers to the ongoing process of scaling applications’ resources to match ever-changing requirements, from bandwidth to entirely new machines, your app may be hosted on up to three machines at once with zero-configuration on your end and completely free!

Our load balancers are backed by some of the largest international providers such as Google, Amazon, and Microsoft. This means we have the operating capacity of some of the largest tech-giants today, that’s over one million queries per second! All with a single AnyCast IP address too, so no worrying about DNS.

Finally, our load balancers are optimised to work with a wide range of traffic types and modern protocols, here are just some of our supported types: HTTPS, TCP/SSL, UDP, HTTP/2, gRPC, and even QUIC!
